<?php


function migrate_i18n_admin() {
  drupal_set_title(t('Migration i18n settings'));
  return drupal_get_form('migrate_i18n_admin_form');
}


function migrate_i18n_admin_form() {
  $languages_source = array();
  $migrate_i18n_map = variable_get('migrate_i18n_map', array());
  $form['migrate_i18n_contentsets'] = array(
    '#type' => 'fieldset',
    '#title' => 'Content Set Mappings',
    '#description' => 'Identify the fields that hold the language of each item in the original data, and the common ID that links the various language versions together as a single "translation".',
    '#tree' => TRUE,
  );
  
  // for each content set, identify the language field and tnid key
  $result = db_query("SELECT * FROM {migrate_content_sets}");
  while ($content_set = db_fetch_object($result)) {   
    $form['migrate_i18n_contentsets'][$content_set->mcsid] = array(
      '#type' => 'fieldset',
      '#title' => $content_set->description,
    );

    $db_info = _migrate_i18n_get_db_info($content_set->view_name);
    $tabledb     = $db_info['database'];
    $tablename   = $db_info['tablename'];
    $view_fields = $db_info['fields'];

    // save the available language codes in this set for the next admin section
    db_set_active($tabledb);
    if (isset($migrate_i18n_map[$content_set->mcsid]['field_lang'])) {
      $sql = "SELECT DISTINCT `". $migrate_i18n_map[$content_set->mcsid]['field_lang'] ."` AS lang FROM {" . $tablename . "}";
      $lang_result = db_query($sql, $migrate_i18n_map[$content_set->mcsid]['field_lang']);
      while ($lang_row = db_fetch_object($lang_result)) {
        if (!in_array($lang_row->lang, $languages_source)) $languages_source[$lang_row->lang] = $lang_row->lang;
      }
    }
    db_set_active('default');

    $form['migrate_i18n_contentsets'][$content_set->mcsid]['field_lang'] = array(
      '#type' => 'select',
      '#title' => 'language field',
      '#options' => $view_fields,
      '#default_value' => isset($migrate_i18n_map[$content_set->mcsid]['field_lang'])? $migrate_i18n_map[$content_set->mcsid]['field_lang'] : NULL,
    );
    
    $form['migrate_i18n_contentsets'][$content_set->mcsid]['field_tnid'] = array(
      '#type' => 'select',
      '#title' => 'translation identifier',
      '#options' => $view_fields,
      '#default_value' => isset($migrate_i18n_map[$content_set->mcsid]['field_tnid'])? $migrate_i18n_map[$content_set->mcsid]['field_tnid'] : NULL,
    );
  }

  // language mapping
  $lang_map = variable_get('migrate_i18n_lang_map', array());
  foreach ($lang_map as $orig=>$i18n) {
    $lang_options[$i18n][$orig] = $orig;
  }
  $form['migrate_i18n_lang_map'] = array(
    '#type' => 'fieldset',
    '#title' => 'Languages',
    '#description' => 'Select which language codes in the original data correspond to the languages enabled by the Internationalization module.',
    '#tree' => TRUE,
  );
  if (!empty($languages_source)) {
    $i18n_languages = i18n_language_list();
    foreach ($i18n_languages as $lang_id=>$lang_name) {
      $form['migrate_i18n_lang_map'][$lang_id] = array(
        '#type' => 'select',
        '#multiple' => TRUE,
        '#title' => $lang_name,
        '#options' => $languages_source,
        '#default_value' => isset($lang_options[$lang_id])? $lang_options[$lang_id]: NULL,
      );
    }
  }
  else {
    $form['migrate_i18n_lang_map']['no_languages'] = array(
      '#value' => 'No language fields defined yet.',
    );
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Submit"),
  );

  // save this for use on submit
  variable_set('migrate_i18n_languages_source', $languages_source);

  return $form;
  
}

// TODO
function migrate_i18n_admin_form_validate ($form, &$form_state) {
  return;
}


function migrate_i18n_admin_form_submit ($form, &$form_state) {
  $nodupes = $lang_map = $new_map = array();
  
  // save tnid key info from form (and tablename from view)
  foreach ($form_state['values']['migrate_i18n_contentsets'] as $mcsid=>$mapping) {
    $result = db_query("SELECT view_name FROM {migrate_content_sets} WHERE mcsid = %d", $mcsid);
    if ($content_set = db_fetch_object($result)) {
      $db_info = _migrate_i18n_get_db_info($content_set->view_name);
      $new_map['tablename']  = $db_info['tablename'];
      $new_map['field_lang'] = $form_state['values']['migrate_i18n_contentsets'][$mcsid]['field_lang'];
      $new_map['field_tnid'] = $form_state['values']['migrate_i18n_contentsets'][$mcsid]['field_tnid'];
      $map[$mcsid] = $new_map;
    }
  }
  variable_set('migrate_i18n_map', $map);

  // language code mapping
  $languages_source = variable_get('migrate_i18n_languages_source', array());
  foreach ($form_state['values']['migrate_i18n_lang_map'] as $i18n_language=>$values) {
    foreach ($values as $value) {
      if (!in_array($value, $nodupes)) {
        $lang_map[$languages_source[$value]] = $i18n_language;
        $nodupes[] = $value;
      }
      else {
        form_set_error('migrate_i18n_lang_map', 'You cannot assign the same content language code to two i18n languages.');
      }
    }
  }
  variable_set('migrate_i18n_lang_map', $lang_map);
}
